# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'new_fs_dialog.ui'
#
# Created: Sun Jan  8 01:57:39 2017
#      by: pyside-uic 0.2.15 running on PySide 1.2.4
#
# WARNING! All changes made in this file will be lost!

from PySide import QtCore, QtGui

class Ui_NewFsDialog(object):
    def setupUi(self, NewFsDialog):
        NewFsDialog.setObjectName("NewFsDialog")
        NewFsDialog.resize(843, 442)
        self.centralwidget = QtGui.QWidget(NewFsDialog)
        self.centralwidget.setObjectName("centralwidget")
        self.gridLayout = QtGui.QGridLayout(self.centralwidget)
        self.gridLayout.setObjectName("gridLayout")
        self.labelTitle = QtGui.QLabel(self.centralwidget)
        font = QtGui.QFont()
        font.setFamily("Helvetica")
        font.setPointSize(15)
        font.setWeight(75)
        font.setBold(True)
        self.labelTitle.setFont(font)
        self.labelTitle.setAlignment(QtCore.Qt.AlignCenter)
        self.labelTitle.setObjectName("labelTitle")
        self.gridLayout.addWidget(self.labelTitle, 0, 0, 1, 4)
        self.labelInputName = QtGui.QLabel(self.centralwidget)
        self.labelInputName.setObjectName("labelInputName")
        self.gridLayout.addWidget(self.labelInputName, 1, 0, 1, 1)
        self.leFsName = QtGui.QLineEdit(self.centralwidget)
        self.leFsName.setObjectName("leFsName")
        self.gridLayout.addWidget(self.leFsName, 2, 0, 1, 4)
        self.labelOptions = QtGui.QLabel(self.centralwidget)
        self.labelOptions.setObjectName("labelOptions")
        self.gridLayout.addWidget(self.labelOptions, 3, 0, 1, 1)
        self.checkBox = QtGui.QCheckBox(self.centralwidget)
        self.checkBox.setObjectName("checkBox")
        self.gridLayout.addWidget(self.checkBox, 4, 0, 1, 4)
        self.cbSparse = QtGui.QCheckBox(self.centralwidget)
        self.cbSparse.setObjectName("cbSparse")
        self.gridLayout.addWidget(self.cbSparse, 5, 0, 1, 4)
        self.cbOptions = QtGui.QCheckBox(self.centralwidget)
        self.cbOptions.setObjectName("cbOptions")
        self.gridLayout.addWidget(self.cbOptions, 6, 0, 1, 4)
        self.leOptionList = QtGui.QLineEdit(self.centralwidget)
        self.leOptionList.setObjectName("leOptionList")
        self.gridLayout.addWidget(self.leOptionList, 7, 0, 1, 4)
        self.btnCancel = QtGui.QPushButton(self.centralwidget)
        self.btnCancel.setObjectName("btnCancel")
        self.gridLayout.addWidget(self.btnCancel, 8, 1, 1, 1)
        self.btnReset = QtGui.QPushButton(self.centralwidget)
        self.btnReset.setObjectName("btnReset")
        self.gridLayout.addWidget(self.btnReset, 8, 2, 1, 1)
        self.btnSubmit = QtGui.QPushButton(self.centralwidget)
        self.btnSubmit.setObjectName("btnSubmit")
        self.gridLayout.addWidget(self.btnSubmit, 8, 3, 1, 1)
        NewFsDialog.setCentralWidget(self.centralwidget)

        self.retranslateUi(NewFsDialog)
        QtCore.QMetaObject.connectSlotsByName(NewFsDialog)

    def retranslateUi(self, NewFsDialog):
        NewFsDialog.setWindowTitle(QtGui.QApplication.translate("NewFsDialog", "MainWindow", None, QtGui.QApplication.UnicodeUTF8))
        self.labelTitle.setText(QtGui.QApplication.translate("NewFsDialog", "Create new file system.  Please see BEZ manual, particularly\n"
" the \'man zfs\' section.", None, QtGui.QApplication.UnicodeUTF8))
        self.labelInputName.setText(QtGui.QApplication.translate("NewFsDialog", "Input new file system name:", None, QtGui.QApplication.UnicodeUTF8))
        self.leFsName.setPlaceholderText(QtGui.QApplication.translate("NewFsDialog", "see documentation for legal file system name format.", None, QtGui.QApplication.UnicodeUTF8))
        self.labelOptions.setText(QtGui.QApplication.translate("NewFsDialog", "Options:", None, QtGui.QApplication.UnicodeUTF8))
        self.checkBox.setText(QtGui.QApplication.translate("NewFsDialog", " -P  Create all non-existion parent datasets as file systems.", None, QtGui.QApplication.UnicodeUTF8))
        self.cbSparse.setText(QtGui.QApplication.translate("NewFsDialog", " -s  Create sparse volume by omitting automatic creation of a refreservation.", None, QtGui.QApplication.UnicodeUTF8))
        self.cbOptions.setText(QtGui.QApplication.translate("NewFsDialog", " -o property=value, where values are given in a comma-separated list below.", None, QtGui.QApplication.UnicodeUTF8))
        self.leOptionList.setPlaceholderText(QtGui.QApplication.translate("NewFsDialog", "comma-separated list of values if -o is checked above.", None, QtGui.QApplication.UnicodeUTF8))
        self.btnCancel.setText(QtGui.QApplication.translate("NewFsDialog", "Cancel", None, QtGui.QApplication.UnicodeUTF8))
        self.btnReset.setText(QtGui.QApplication.translate("NewFsDialog", "Reset", None, QtGui.QApplication.UnicodeUTF8))
        self.btnSubmit.setText(QtGui.QApplication.translate("NewFsDialog", "Submit", None, QtGui.QApplication.UnicodeUTF8))

