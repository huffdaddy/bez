# System imports
import configparser
import os
import platform
import re
import subprocess
import sys
import threading
import time
import PySide
from PySide import QtCore
from PySide import QtGui
from PySide.QtCore import *
from PySide.QtGui import *
from weir import zfs

import BEZ_Ui
import new_fs_dialog
import pool_import

# Some package constants
__version__ = '0.0.1'


class WorkerThread(QThread):
    def __init__(self, function):
        QThread.__init__(self)
        self.function = function

    # def __del__(self):
        # self.wait()

    def run(self):
        secs = 0
        while True:
            secs += 1
            time.sleep(1)
            print(secs)
        self.function()
        return

class NewFileSystem(QMainWindow, new_fs_dialog.Ui_NewFsDialog):
    """Create new filesystem via pop-up dialog."""

    def __init__(self, parent=None):
        super(NewFileSystem, self).__init__(parent)
        self.setupUi(self)
        self.btnCancel.clicked.connect(self.dead)
        self.btnSubmit.clicked.connect(self.create_fs)

    def create_fs(self):
        """Either create new file system or call 'message_failed_import'."""
        new_fs = self.leFsName.text()
        try:
            zfs.create(new_fs)
        except:
            self.message_failed_import()

    def message_failed_import(self):
        """Display Pop-up message, tell user toa try again."""
        QMessageBox.about(self, "About PySide, Platform and version.",
                          """<b><c>Creation failed.  Please try again.</c> </b>""")

    def dead(self):
        self.destroy()


class PoolImport(QMainWindow, pool_import.Ui_poolImport):
    """
    Prompt user with pop-up for pool import options.

    User options: -d, -N, -R, Abort.

    """

    def __init__(self, parent=None):
        super(PoolImport, self).__init__(parent)
        self.setAttribute(Qt.WA_DeleteOnClose)
        self.setupUi(self)

        self.config = configparser.ConfigParser()
        self.config_file = '/home/' + os.getenv('SUDO_USER') + '/.BEZconf'

        self.btnAbort.clicked.connect(self.dead)
        self.btnImport.clicked.connect(self.get_opts)

    def get_opts(self):
        import_opts = []
        import_opts.append(str(" "))
        self.config.read(self.config_file)
        self.opt_pool = self.config.get("Zpool import parameters", "pool")
        if self.ckboxOptionN.isChecked():
            import_opts.append(str('-N '))
        if len(self.ledOption.text()) > 0:
            import_opts.append(' -d ' + self.ledOption.text())
            self.pool_import(import_opts)
        if len(self.leROption.text()) > 0:
            import_opts.append(' -R ' + self.leROption.text())
        self.pool_import(import_opts[0])

    def pool_import(self, opts):
        """Import pool with options from 'get_opts', if any."""
        subprocess.getoutput(
            'zpool import ' + opts + self.opt_pool)

        self.dead()

    def dead(self):
        self.destroy()


class MainZ(QMainWindow, BEZ_Ui.Ui_BEZ):

    """The Big Kahuna, the _main program_. """

    # -------------------------------------------------------------------------
    def __init__(self, parent=None):
        super(MainZ, self).__init__(parent)
        self.setupUi(self)

        # Set initial view
        commands_pool_stats = ['arc_summary', 'arcstat', 'zdb', 'zpool events',
                               'zpool iostat']
        for stat in commands_pool_stats:
            self.cbCommandStats.addItem(stat)

        # Set initial variables
        self.init = 1
        self.doc_overview_url = "./docs/bez_overview.html"
        self.doc_manual_url = "./docs/bez_manual.html"
        self.webViewDocumentation.setUrl(QtCore.QUrl(self.doc_overview_url))
        self.webViewManual.setUrl(QtCore.QUrl(self.doc_manual_url))
        self.config = configparser.ConfigParser()
        self.setting_file = '/home/' + os.getenv('SUDO_USER') + '/.BEZconf'

        # Set initial view
        self.mainTabWidget.setCurrentIndex(0)

        # Create models
        self.model_listImportedPoolsMain = QStandardItemModel(self)
        self.listImportedPoolsMain.setModel(self.model_listImportedPoolsMain)

        self.model_listExportedPoolsMain = QStandardItemModel(self)
        self.listExportedPoolsMain.setModel(self.model_listExportedPoolsMain)

        self.model_tableFssMain = QStandardItemModel(0, 2)
        self.tableFssMain.setModel(self.model_tableFssMain)

        self.model_tablePoolPools = QStandardItemModel(0, 1)
        self.tablePoolPools.setModel(self.model_tablePoolPools)

        self.model_listFssFss = QStandardItemModel(self)
        self.listFssFss.setModel(self.model_listFssFss)

        self.model_tableFsPropsFss = QStandardItemModel(self)
        self.tableFsPropsFss.setModel(self.model_tableFsPropsFss)

        self.model_listFssSnaps = QStandardItemModel(self)
        self.listFssSnaps.setModel(self.model_listFssSnaps)

        self.model_listSnapsSnaps = QStandardItemModel(self)
        self.listSnapsSnaps.setModel(self.model_listSnapsSnaps)

        # Set menu actions
        self.actionMainPage.triggered.connect(self.show_main_tab)
        self.actionPool.triggered.connect(self.show_pools_tab)
        self.actionFilesystem.triggered.connect(self.show_filesystems_tab)
        self.actionSnapshots.triggered.connect(self.show_snapshots_tab)
        self.actionClones.triggered.connect(self.show_clones_tab)
        self.actionStats.triggered.connect(self.show_stats_tab)
        self.actionHistory.triggered.connect(self.show_history_tab)
        self.actionHelp.triggered.connect(self.show_help_tab)
        self.actionDocumentationNavigateMenu.triggered.connect(
            self.show_documentation_tab)
        self.actionDocumentationHelpMenu.triggered.connect(
            self.show_documentation_tab)
        self.actionPreferences.triggered.connect(self.show_prefs_tab)
        self.actionAbout_Z4Me.triggered.connect(self.about_pu)
        self.actionGeneral_Help.triggered.connect(self.show_help_tab)
        self.actionDocumentationHelpMenu.triggered.connect(
            self.show_documentation_tab)
        self.actionLicensing_and_Usage.triggered.connect(
            self.show_documentation_tab)
        self.actionZ4Me_Website.triggered.connect(self.show_documentation_tab)
        self.actionQuit.triggered.connect(self.quit_application)

        # Set other actionsselected_fs
        self.btnUnmountMain.clicked.connect(self.confirm_unmount)
        self.btnMountMain.clicked.connect(self.mount_filesystem)
        self.btnExportMain.clicked.connect(self.confirm_export)
        self.btnImportMain.clicked.connect(self.import_pool_dialog)
        self.btnRefreshMain.clicked.connect(self.set_main_view)
        self.btnHelpMain.clicked.connect(self.show_help_tab)
        self.cbPoolsPools.activated.connect(self.update_table_pools_pools)
        self.cbPoolsSnaps.activated.connect(self.update_list_fss_snaps)
        self.listFssFss.clicked.connect(self.update_table_fs_parameters)
        self.listFssFss.activated.connect(self.update_table_fs_parameters)
        self.cbPoolsFss.activated.connect(self.update_list_fss_fss)
        self.btnClearSnaps.clicked.connect(self.update_list_Fss_snaps)
        self.listFssSnaps.activated.connect(self.update_list_Fss_snaps)
        self.listFssSnaps.clicked.connect(self.update_list_Fss_snaps)
        self.btnDeleteSnaps.clicked.connect(self.confirm_delete_snaps)
        self.cbCommandStats.activated.connect(self.get_stat_command_opts)
        self.btnDisplayHistory.clicked.connect(self.update_history)
        self.btnResetRefreshPrefs.clicked.connect(self.reset_prefs_refresh_rate)
        self.btnSavePrefs.clicked.connect(self.set_prefs)

        # Split startup into threads
        imported_pool_thread = threading.Thread(target=self.poll_pools)
        imported_pool_thread.setDaemon(True)
        imported_pool_thread.start()

        mounted_fss_main_thread = threading.Thread(
            target= self.update_list_fss_main)
        mounted_fss_main_thread.setDaemon(True)
        mounted_fss_main_thread.start()

        fs_mounted_poll_thread = threading.Thread(
            target = self.poll_list_fss_mounted_main)
        fs_mounted_poll_thread.setDaemon(True)
        fs_mounted_poll_thread.start()

        self.update_table_pools_pools()

        self.show()
        self.set_main_view()

    def poll_pools(self):
        while 1:
            self.config.read(self.setting_file)
            refresh_minutes = int(
                self.config.get('Refresh rates', 'poolminutes'))
            refresh_seconds = int(
                self.config.get('Refresh rates', 'poolseconds'))
            refresh_time = (refresh_minutes * 60) + (refresh_seconds)
            imported_pools = self.get_imported_pools()
            exported_pools = self.get_exported_pools()
            time.sleep(0)
            if self.init < 1: time.sleep(refresh_time)

            if len(
                imported_pools
                ) != self.model_listImportedPoolsMain.rowCount():
                self.model_listImportedPoolsMain.clear()
                for imported_pool in imported_pools:
                    self.model_listImportedPoolsMain.appendRow(
                        QStandardItem(imported_pool))

            if len(
                exported_pools
                ) != self.model_listExportedPoolsMain.rowCount():
                self.model_listExportedPoolsMain.clear()
                for exported_pool in exported_pools:
                    self.model_listExportedPoolsMain.appendRow(
                        QStandardItem(exported_pool))

    def poll_list_fss_mounted_main(self, *refresh_time):
        while 1:
            self.config.read(self.setting_file)
            refresh_minutes = int(
                self.config.get('Refresh rates', 'fsminutes'))
            refresh_seconds = int(
                self.config.get('Refresh rates', 'fsseconds'))
            refresh_time = (refresh_minutes * 60) + (refresh_seconds)
            if self.init == 1: refresh_time = 0.1
            try:
                x = 1 / refresh_time
                time.sleep(refresh_time)
                fs_row = 0
                pools = self.get_imported_pools()
                for pool in pools:
                    for fs in zfs.find(pool):
                        index2 = self.model_tableFssMain.index(
                            fs_row, 2, QModelIndex())
                        self.model_tableFssMain.setData(
                            index2, zfs.findprops(fs.name)[6]['value'])
                        fs_row += 1
            except:
                break

    def set_main_view(self):
        """
        Set dynamic views of program.

        Don't do anything except call modules that display dynamic views
        of program.  Dynamic views are defined as any portion of display
        that changes with user input.


        """

        self.update_cbs_imported_pools()
        self.update_cb_fss_clones()
        self.update_list_fss_snaps()
        self.update_list_fss_mounted_main()
        self.init = 0

    # Menu Functions
    # -------------------------------------------------------------------------
    def show_main_tab(self):
        """Set current view to the 'Main' tab."""
        self.mainTabWidget.setCurrentIndex(0)

    def show_pools_tab(self):
        """Set view to the 'Pools' tab."""
        self.mainTabWidget.setCurrentIndex(1)

    def show_filesystems_tab(self):
        """Set view to the 'Filesystems' tab."""
        self.mainTabWidget.setCurrentIndex(2)

    def show_snapshots_tab(self):
        """ Set current view to the 'Snapshots' tab. """

        self.mainTabWidget.setCurrentIndex(3)

    def show_clones_tab(self):
        """Set current view to the 'Clones' tab."""
        self.mainTabWidget.setCurrentIndex(4)

    def show_stats_tab(self):
        """Set current view to the 'Stats' tab."""
        self.mainTabWidget.setCurrentIndex(5)

    def show_history_tab(self):
        """Set current view to the 'History' tab."""
        self.mainTabWidget.setCurrentIndex(6)


    def show_help_tab(self):
        """ Set current view to the 'Help' tab. """
        self.mainTabWidget.setCurrentIndex(7)

    def show_documentation_tab(self):
        """ Set current view to the 'Documentation' tab. """
        self.mainTabWidget.setCurrentIndex(8)

    def show_prefs_tab(self):
        """Set current view to the 'Prefs' tab."""
        self.mainTabWidget.setCurrentIndex(9)

    # -------------------------------------------------------------------------
    # Display modules main - create or update contents
    # of widgets or models
    # -------------------------------------------------------------------------
    def update_cbs_imported_pools(self):
        """
        Update contents of combo-boxes.

        Only combo-boxes containing lists of imported pools are updated.

        """

        self.cbPoolsPools.clear()
        self.cbPoolsFss.clear()
        self.cbPoolsSnaps.clear()
        self.cbPoolsClones.clear()
        self.cbPoolStats.clear()
        self.cbPoolHistory.clear()
        for pool in self.get_imported_pools():
            self.cbPoolsPools.addItem(pool)
            self.cbPoolsFss.addItem(pool)
            self.cbPoolsSnaps.addItem(pool)
            self.cbPoolsClones.addItem(pool)
            self.cbPoolStats.addItem(pool)
            self.cbPoolHistory.addItem(pool)

    def update_cb_fss_clones(self):
        self.cbFssClones.clear()
        fs_iterator = 0
        selected_pool = self.cbPoolsClones.currentText()
        for fs in zfs.find(selected_pool):
            self.cbFssClones.addItem(fs.name)


    def update_list_imported_pools_main(self):
        """ Update list of imported pools on the 'Main' tab. """

        self.model_listImportedPoolsMain.clear()

        for pool in self.get_imported_pools():
            self.model_listImportedPoolsMain.appendRow(
                QStandardItem(pool))

    def update_list_exported_pools_main(self):
        """ Update list of exported pools on the 'Main' tab. """

        self.model_listExportedPoolsMain.clear()
        exported_pools = self.get_exported_pools()
        for pool in exported_pools:
            self.model_listExportedPoolsMain.appendRow(
                QStandardItem(pool))
            self.listExportedPoolsMain.setModel(
                self.model_listExportedPoolsMain)


    def update_list_fss_main(self):
        """
        List all filesystems for currently mounted pools on the 'Main' tab.

        """

        self.model_tableFssMain.setRowCount(0)
        self.model_tableFssMain.setHorizontalHeaderLabels(
            ['Filesystem', 'Mount point', 'Mounted'])

        fs_row = 0
        pools = self.get_imported_pools()
        for pool in pools:
            for fs in zfs.find(pool):
                self.model_tableFssMain.appendRow(QStandardItem(fs.name))

                index1 = self.model_tableFssMain.index(
                    fs_row, 1, QModelIndex())

                self.model_tableFssMain.setData(
                    index1, zfs.findprops(fs.name)[10]['value'])

                fs_row += 1

        self.tableFssMain.horizontalHeader().setResizeMode(
            0, QHeaderView.Stretch)
        self.tableFssMain.horizontalHeader().setResizeMode(
            1, QHeaderView.Stretch)

    def update_list_fss_mounted_main(self):
        fs_row = 0
        pools = self.get_imported_pools()
        for pool in pools:
            for fs in zfs.find(pool):
                index2 = self.model_tableFssMain.index(
                    fs_row, 2, QModelIndex())
                self.model_tableFssMain.setData(
                    index2, zfs.findprops(fs.name)[6]['value'])
                fs_row += 1

    def update_table_pools_pools(self):
        """
        Populate table view on the 'Pools' tab with all parameters.

        Same as available via the 'zpool get' command.  subprocess module
        is called rather than weir.zfs as functionality of the later is not yet
        fully understood by this author.

        """

        self.model_tablePoolPools.setRowCount(0)
        selected_pool = self.cbPoolsPools.currentText()

        poolprops = self.get_zpool_props()
        for prop in poolprops:
            prop = subprocess.getoutput(
                "zpool get " + prop + " " + selected_pool + " |grep -v ^NAME")
            split_prop = re.split(r'\s *', prop)
            self.model_tablePoolPools.appendRow(QStandardItem(split_prop[2]))
        self.model_tablePoolPools.setVerticalHeaderLabels(
            self.get_zpool_props())

    def update_list_fss_fss(self):
        self.model_listFssFss.setRowCount(0)
        fs_iterator = 0
        selected_pool = self.cbPoolsFss.currentText()
        for fs in zfs.find(selected_pool):
            self.model_listFssFss.appendRow(QStandardItem(
                fs.name))

    def update_table_fs_parameters(self):
        self.model_tableFsPropsFss.setRowCount(0)
        row = self.listFssFss.currentIndex().row()
        qrow_value = self.model_listFssFss.data(
            QModelIndex(self.model_listFssFss.index(row, 0)))
        self.populate_fs_details_table(qrow_value)

    def update_list_Fss_snaps(self):
        self.model_listSnapsSnaps.setRowCount(0)
        row = self.listFssSnaps.currentIndex().row()
        qrow_value = self.model_listFssSnaps.data(
            QModelIndex(self.model_listFssSnaps.index(row, 0)))
        self.update_list_snaps_snaps(qrow_value)

    def update_list_snaps_snaps(self, filesystem):
        snaps = zfs.open(filesystem).snapshots()
        snap_index = 0
        if len(snaps) > 0:
            for snap in snaps:
                self.model_listSnapsSnaps.appendRow(QStandardItem(snap.name))
        else:
            self.model_listSnapsSnaps.appendRow(QStandardItem('None'))

    def populate_fs_details_table(self, filesystem):
        self.model_tableFsPropsFss.setRowCount(0)
        self.model_tableFsPropsFss.setVerticalHeaderLabels(
            self.get_zfs_proplist())

        for zvalue_list in range(0, 62):
            try:
                item_value = zfs.open(
                    filesystem).getprops()[zvalue_list]['value']
                self.model_tableFsPropsFss.setItem(
                    zvalue_list, QStandardItem(item_value))
            except:
                break

    def update_list_fss_snaps(self):
        """Display list of snapshots for selected filesystem."""
        self.model_listFssSnaps.setRowCount(0)
        selected_pool = self.cbPoolsSnaps.currentText()
        for fs in zfs.find(selected_pool):
            self.model_listFssSnaps.appendRow(
                QStandardItem(fs.name))

    def update_history(self):
        """Display command history of selected pool."""
        self.textBrowserHistory.clear()
        opt_i = ' '
        opt_l = ' '
        pool = self.cbPoolHistory.currentText()
        if self.ckbiHistory.isChecked(): opt_i = ' -i '
        if self.ckblHistory.isChecked(): opt_l = ' -l '
        history_command = subprocess.getoutput(
            'zpool history' + opt_i + opt_l + pool)
        show_history_thread = threading.Thread(
            target=self.textBrowserHistory.append(history_command))
        show_history_thread.start()

    # Non-Action Functions
    # -------------------------------------------------------------------------

    def get_zfs_proplist(self):
       """
       Return list type containing zfs properties.

       Module not called directly but by 'fs_update_table'.  weir.zfs
       retrieves all zfs properties except user-defined.

       """

       fs_prop_list = ['type', 'creation', 'used', 'available', 'referenced',
                       'compressratio', 'mounted', 'quota', 'reservation',
                       'recordsize', 'mountpoint', 'sharenfs', 'checksum',
                       'compression', 'atime', 'devices', 'exec', 'setuid',
                       'readonly', 'zoned', 'snapdir', 'aclinherit',
                       'canmount', 'xattr', 'copies', 'version', 'utf8only',
                       'normalization', 'casesensitivity', 'vscan', 'nbmand',
                       'sharesmb', 'refquota', 'refreservation',
                       'primarycache', 'secondarycache', 'usedbysnapshots',
                       'usedbydataset', 'usedbychildren',
                       'usedbyrefreservation', 'logbias', 'dedup', 'mlslabel',
                       'sync', 'dnodesize', 'refcompressratio', 'written',
                       'logicalused', 'logicalreferenced', 'filesystem_limit',
                       'snapshot_limit', 'filesystem_count', 'snapshot_count',
                       'snapdev', 'acltype', 'context', 'fscontext',
                       'defcontext', 'rootcontext', 'relatime',
                       'redundant_metadata', 'overlay']
       return fs_prop_list

    def get_imported_pools(self):
        """
        Return list of all imported pools as 'imported_pools'.

        Uses the subprocess module as this author cannont determine
        whether weir.zfs provides this functionality.

        """

        pool_string = subprocess.getoutput(
            'zpool list |grep -v ^NAME.*SIZE.*ALLOC |grep -o ^[a-Z0-9]*')
        imported_pools = re.split(r'\s *', pool_string)
        return imported_pools

    def get_exported_pools(self):
        """
        Return list of pools available for import as a list, 'exported_pools'.

        Uses the subprocess module as this author cannont determine whether
        weir.zfs provides this functionality.

        """

        pool_string = subprocess.getoutput(
            'zpool import |grep ^[[:space:]]*pool: |grep -o [a-Z0-9_-]*$'
        )
        exported_pools = re.split(r'\s *', pool_string)
        return exported_pools

    def get_zpool_props(self):
        """Return list type containing list of all zpool properties."""

        zpool_prop_list = ['capacity', 'altroot', 'health', 'guid', 'version',
                           'bootfs', 'delegation', 'autoreplace', 'cachefile',
                           'failmode', 'listsnapshots', 'autoexpand',
                           'dedupditto', 'dedupratio', 'free', 'allocated',
                           'readonly', 'ashift', 'comment', 'expandsize',
                           'freeing', 'fragmentation', 'leaked',
                           'feature@async_destroy', 'feature@empty_bpobj',
                           'feature@lz4_compress', 'feature@spacemap_histogram',
                           'feature@enabled_txg', 'feature@hole_birth',
                           'feature@extensible_dataset', 'feature@embedded_data',
                           'feature@bookmarks', 'feature@filesystem_limits',
                           'feature@large_blocks', 'feature@large_dnode']
        return zpool_prop_list

    def get_stat_command_opts(self):
        if self.cbCommandStats.currentIndex() == 0:
            opt = ['None']
            self.leOptionsStats.setText('None')
            self.leOptionsStats.setToolTip(
                'No options available for arc_summary.')
        else:
            self.leOptionsStats.clear()
            self.leOptionsStats.clear()



    # Action functions.
    # -------------------------------------------------------------------------
    def import_pool_dialog(self):
        self.config.read(self.setting_file)
        selected_pool = self.listExportedPoolsMain.selectedIndexes()
        opt_pool = self.model_listExportedPoolsMain.data(selected_pool[0])
        self.config.set("Zpool import parameters", "Pool", opt_pool)
        cfgfile = open(self.setting_file, 'w')
        self.config.write(cfgfile)
        cfgfile.close()
        PoolImport(self).show()

    def import_pool(self):
        """
        Import selected pools from 'exported pools' list.

        Method imports pools selected from the 'Uinimported pools'
        list on the 'Main' tab interface, using the 'Import pool' button.  The
        subprocess module is used as the weir.zfs as functionality of the later
        is not yet fully understood by this author.

        """

        try:
            selected_pool = self.listExportedPoolsMain.selectedIndexes()
            selected_index = self.model_listExportedPoolsMain.data(
                selected_pool[0])
            subprocess.getoutput('zpool import ' + selected_index)
        except:
            pass
        self.update_cbs_imported_pools()
        self.model_tableFssMain.setRowCount(0)
        self.update_list_fss_main()
        self.update_list_fss_mounted_main()

    def update_main(self):
        self.update_cbs_imported_pools()
        self.update_list_fss_main()

    def mount_filesystem(self):
        """
        Mount selected filesystems.

        Subprocess module is used here rather than weir.zfs as this
        author does not understand whether this is supported by the latter.

        """

        try:
            selected_fs = self.tableFssMain.selectedIndexes()
            selected_index = self.model_tableFssMain.data(selected_fs[0])
            subprocess.getoutput('zfs mount ' + selected_index)
        except:
            pass
        self.model_tableFssMain.setRowCount(0)
        self.update_list_fss_main()
        self.update_list_fss_mounted_main()

    def unmount_filesystem(self):
        """
        Unmount selected filesystems.

        Subprocess module is used here rather than weir.zfs as this
        author does not understand whether this is supported by the latter.

        """

        try:
            selected_fs = self.tableFssMain.selectedIndexes()
            selected_index = self.model_tableFssMain.data(selected_fs[0])
            subprocess.getoutput('zfs unmount ' + selected_index)
        except:
            pass
        self.model_tableFssMain.setRowCount(0)
        self.update_list_fss_main()
        self.update_list_fss_mounted_main()

    def destroy_pool(self, pool):
        zfs.open(pool).destroy()

    def set_prefs(self):
        cfgfile = open(self.setting_file, 'w')
        # self.config.read(cfgfile)
        self.config.set(
            "Refresh rates", "poolminutes", str(self.sbPoolMinutesPrefs.value()))
        self.config.set(
            "Refresh rates", "poolseconds", str(self.sbPoolSecondsPrefs.value()))
        self.config.set(
            "Refresh rates", "fsminutes", str(self.sbFsMinutesPrefs.value()))
        self.config.set(
            "Refresh rates", "fsseconds", str(self.sbFsSecondsPrefs.value()))

        self.config.write(cfgfile)
        cfgfile.close()

    def reset_prefs_refresh_rate(self):
        self.sbPoolMinutesPrefs.setValue(0)
        self.sbPoolSecondsPrefs.setValue(30)
        self.sbFsMinutesPrefs.setValue(0)
        self.sbFsSecondsPrefs.setValue(0)

    # Display modules pop-ups
    # -------------------------------------------------------------------------

    def documentation_cddl(self):
        """Read and display licence."""
        # with open('CCPL.txt') as nonamefile:
        # self.textBrowserDocumentation.setText(nonamefile.read())
        pass

    def about_pu(self):
        """Display Pop-up message with 'about BEZ' text."""

        # noinspection PyCallByClass
        QMessageBox.about(self, "About PySide, Platform and version.",
                          """<b><c>ZGA version %s</c> </b>
            <p>Copyright &copy; 2017 by Bill Huffman.
            This work is made available under  the terms of
            Creative Commons Attribution-ShareAlike 3.0 license,
            http://creativecommons.org/licenses/by-sa/3.0/.
            <p>Python %s -  PySide version %s - Qt version %s on %s""" %
                          (__version__, platform.python_version(),

                           PySide.__version__, PySide.QtCore.__version__,

                           platform.system()))


    def confirm_export(self):
        """
        Display pop-up message confirming pool export.

        Query user for confirmation before calling 'export_pool.

        """

        flags = QtGui.QMessageBox.StandardButton.Ok
        flags |= QtGui.QMessageBox.StandardButton.Abort
        question = open("./docs/export_question.txt").read()
        response = QtGui.QMessageBox.question(
            self, "Please Confirm",
            question,
            flags)
        if response == QtGui.QMessageBox.Ok:
            self.export_pool()
        else:
            pass
        self.update_cbs_imported_pools()

    def confirm_unmount(self):
        """Display pop-up confirming the unmount of selected filesystem."""

        flags = QtGui.QMessageBox.StandardButton.Ok
        flags |= QtGui.QMessageBox.StandardButton.Abort
        question = open("./docs/unmount_question.txt").read()
        response = QtGui.QMessageBox.question(self, "Please Confirm",
                                              question,
                                              flags)
        if response == QtGui.QMessageBox.Ok:
            self.unmount_filesystem()
        else:
            pass

    def confirm_delete_snaps(self):
        """ Show message to confirm deletion of selected snapshots. """

        flags = QtGui.QMessageBox.StandardButton.Ok
        flags |= QtGui.QMessageBox.StandardButton.Abort
        question = open("./docs/delete_snaps_question.txt").read()
        response = QtGui.QMessageBox.question(self, "Please Confirm",
                                              question,
                                              flags)
        if response == QtGui.QMessageBox.Ok:
            self.delete_snaps()
        else:
            pass

    def confirm_delete_fss(self):
        """ Show message to confirm deletion of selected filesystem(s). """

        flags = QtGui.QMessageBox.StandardButton.Ok
        flags |= QtGui.QMessageBox.StandardButton.Abort
        question = open("./docs/delete_snaps_question.txt").read()
        response = QtGui.QMessageBox.question(self, "Please Confirm",
                                              question,
                                              flags)
        if response == QtGui.QMessageBox.Ok:
            self.delete_fss()
        else:
            pass

    def delete_snaps(self):
        """Delete a selected snapshot(s). """

        selected_snaps = self.listSnapsSnaps.selectedIndexes()
        iterator = 0
        while True:
            try:
                selected_indexes = self.model_listSnapsSnaps.data(
                    selected_snaps[iterator])
                zfs.open(selected_indexes).destroy()
                iterator += 1
            except:
                break
        self.update_cbs_imported_pools()
        self.update_main()

    def delete_fss(self):
        """Delete a selected filesystem(s). """

        selected_fss = self.tableFssMain.selectedIndexes()
        iterator = 0
        while True:
            try:
                selected_indexes = self.model_tableFssMain.data(
                    selected_fss[iterator])
                zfs.open(selected_indexes).destroy()
                iterator += 1
            except:
                break
        self.update_cbs_imported_pools()

    def export_pool(self):
        """
        Export selected pool.

        Module not accessed directly, but from the 'confirm_export'
        dialog after the user confirmation.  The subprocess module
        is used here rather than weir.zfs as functionality of the later is not
        yet fully understood by this author.

        """

        try:
            selected_pool = self.listImportedPoolsMain.selectedIndexes()
            selected_index = self.model_listImportedPoolsMain.data(
                selected_pool[0])
            subprocess.getoutput('zpool export ' + selected_index)
        except:
            pass
        self.update_cbs_imported_pools()
        self.model_tableFssMain.setRowCount(0)
        self.update_list_fss_main()
        self.update_list_fss_mounted_main()

    # Other misc modules
    # -------------------------------------------------------------------------

    def progress(data, *args):
        it=iter(data)
        widget = QtGui.QProgressDialog(*args+(0,it.__length_hint__()))
        c=0
        for v in it:
            QtCore.QCoreApplication.instance().processEvents()
            if widget.wasCanceled():
                raise StopIteration
            c+=1
            widget.setValue(c)
            yield(v)

    def quit_application(self):
        sys.exit()

# Main program
# ----------------------------------------------------------------------------

app = QApplication(sys.argv)
form = MainZ()
app.exec_()
